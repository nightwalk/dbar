#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <X11/Xlib.h>
#include <time.h>

char *get_time();
char *get_cputemp();
double *get_loadavg(int);

int main(int argc, char *argv[])
{
	Display *d;
	char *time;
	char *cputemp;
	double *loadavg;
	char status[512];

	d = XOpenDisplay(NULL);
	if (d == NULL) {
		return -1;
	}

	while (1) {
		time = get_time();
		cputemp = get_cputemp();
		loadavg = get_loadavg(2);

		snprintf(status, 512, "LOAD: %.2f | CPU TEMP: %s | %s", loadavg[1], cputemp, time);

		XStoreName(d, DefaultRootWindow(d), status);
		XSync(d, False);

		sleep(1);
		free(time);
		free(cputemp);
	}
	
	return 1;
}

char *get_time()
{
	char *ret;
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);

	ret = malloc(26);
	if (ret == NULL) {
		return ret;
	}

	asctime_r(tm, ret);
	ret[24] = '\0';

	return ret;
}

char *get_cputemp()
{
	char *ret;
	ssize_t r;

	int fd;

	ret = malloc(3);
	if (ret == NULL) {
		return ret;
	}

	if (ret != NULL) {
		fd = open("/sys/class/hwmon/hwmon1/temp1_input", O_RDONLY);
		r = read(fd, ret, 2);
		if (r != 2) {
			close(fd);
			return NULL;
		}
		close(fd);

		ret[2] = '\0';
		return ret;
	}

	return ret;
}

double *get_loadavg(int nelem)
{
	double *ret;
	if (nelem > 3) {
		return NULL;
	}

	ret = malloc(sizeof(double) * nelem);
	if (ret == NULL) {
		return ret;
	}

	if (getloadavg(ret, nelem) == -1) {
		free(ret);
		return NULL;
	}

	return ret;
}
